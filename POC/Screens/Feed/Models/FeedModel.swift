//
//  FeedModel.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation

struct FeedModel: Codable {

    let title: String?
    let feeds: [Feed]
 
    enum CodingKeys: String, CodingKey {

        case title
        case feeds = "rows"
    }
}

struct Feed: Codable {
    
    let title: String?
    let description: String?
    let imgUrl: String?
    
    enum CodingKeys: String, CodingKey {

        case title, description
        case imgUrl = "imageHref"
    }
}

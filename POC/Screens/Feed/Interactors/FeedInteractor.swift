//
//  FeedInteractor.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import RxSwift
import Moya

final class FeedInteractor {

    static func getAllFeed() -> Single<FeedModel> {
        return Service.shared.connect(api: APIFactory.allFeed).mapObject(type: FeedModel.self)
    }
}

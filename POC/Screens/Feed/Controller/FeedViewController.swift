//
//  FeedViewController.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import UIKit
import RxSwift

class FeedViewController: UIViewController {

    private let disposeBag: DisposeBag = DisposeBag()
    
    private var viewModel: FeedViewModel!
    
    private lazy var tblVFeed = FeedTableView()
    private lazy var activityIndicatorView = UIActivityIndicatorView()
    private lazy var lblError = UILabel(frame: .zero)

    init(viewModel: FeedViewModel = FeedViewModel()) {
        
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupUI()
        refreshData()
    }
}

extension FeedViewController {
    
    private func setupUI() {
        
        tblVFeed.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tblVFeed)
        
        view.safeAreaLayoutGuide.leadingAnchor.constraint(equalTo: tblVFeed.leadingAnchor).isActive = true
        view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: tblVFeed.trailingAnchor).isActive = true
        view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: tblVFeed.topAnchor).isActive = true
        view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: tblVFeed.bottomAnchor).isActive = true
        
        tblVFeed.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        //...
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.style = .whiteLarge
        activityIndicatorView.color = .lightGray
        
        view.addSubview(activityIndicatorView)
        view.safeAreaLayoutGuide.centerXAnchor.constraint(equalTo: activityIndicatorView.centerXAnchor).isActive = true
        view.safeAreaLayoutGuide.centerYAnchor.constraint(equalTo: activityIndicatorView.centerYAnchor).isActive = true
        
        //...
        lblError.font = HelveticaNeue.regular.size((15.0).proportionalToWidth)
        lblError.translatesAutoresizingMaskIntoConstraints = false
        lblError.textAlignment = .center
        lblError.isHidden = true
        lblError.numberOfLines = 0
        view.addSubview(lblError)
        
        lblError.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: (16.0).proportionalToWidth).isActive = true
        lblError.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: (-16.0).proportionalToWidth).isActive = true
        lblError.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
    }
    
    @objc private func refreshData() {
        
        let isRefreshing = self.tblVFeed.refreshControl?.isRefreshing ?? false
        if !isRefreshing {
            
            self.tblVFeed.isHidden = true
            self.activityIndicatorView.startAnimating()
        }
        
        viewModel.getFeed().asObservable().subscribe(onNext: { (feedModel) in
            
            self.tblVFeed.refreshControl?.endRefreshing()
            self.activityIndicatorView.stopAnimating()
            
            self.title = feedModel.title
            self.tblVFeed.update(feeds: feedModel.feeds)
            
            if feedModel.feeds.count == 0 {
                
                self.tblVFeed.isHidden = true
                self.lblError.isHidden = false
                self.lblError.text = "No data found!"
            } else {
                
                self.tblVFeed.isHidden = false
                self.lblError.isHidden = true
                self.lblError.text = ""
            }
            
        }, onError: { error in
            
            self.tblVFeed.refreshControl?.endRefreshing()
            self.activityIndicatorView.stopAnimating()
            
            self.tblVFeed.isHidden = true
            self.lblError.isHidden = false
            self.lblError.text = error.localizedDescription
        }).disposed(by: disposeBag)
    }
}

//
//  FeedTableView.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import UIKit

class FeedTableView: UITableView {

    private var feeds = [Feed]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    init() {
        
        super.init(frame: .zero, style: .plain)
        
        register(FeedTableViewCell.self, forCellReuseIdentifier: FeedTableViewCell.identifier)
        refreshControl = UIRefreshControl()
        rowHeight = UITableView.automaticDimension
        estimatedRowHeight = 500
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(feeds: [Feed]) {
        
        self.feeds = feeds
        reloadData()
    }
}

extension FeedTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifier, for: indexPath) as? FeedTableViewCell {
            cell.configure(feed: feeds[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
}

//
//  FeedTableViewCell.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import UIKit
import SDWebImage

class FeedTableViewCell: UITableViewCell {

    private let lblTitle = UILabel(frame: .zero)
    private let imgVFeed = UIImageView(frame: .zero)
    private let lblDetail = UILabel(frame: .zero)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        setupTitle()
        setupImage()
        setupDetail()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(feed: Feed) {
        
        lblTitle.text = feed.title ?? "No title!"
        
        if let imgUrl = feed.imgUrl, let url = URL(string: imgUrl) {
            imgVFeed.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgVFeed.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), completed: nil)
        } else {
            imgVFeed.image = UIImage(named: "placeholder")
        }
        
        lblDetail.text = feed.description ?? "No description!"
    }
}

extension FeedTableViewCell {
    
    private func setupTitle() {
        
        //...title
        lblTitle.font = HelveticaNeue.regular.size((15.0).proportionalToWidth)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(lblTitle)
        
        lblTitle.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: (16.0).proportionalToWidth).isActive = true
        lblTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: (16.0).proportionalToWidth).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: (-16.0).proportionalToWidth).isActive = true
    }
    
    private func setupImage() {
        
        imgVFeed.translatesAutoresizingMaskIntoConstraints = false
        imgVFeed.backgroundColor = .white_EBEBEB
        imgVFeed.contentMode = .scaleToFill
        imgVFeed.layer.cornerRadius = (10.0).proportionalToWidth
        imgVFeed.layer.masksToBounds = true
        contentView.addSubview(imgVFeed)
        
        imgVFeed.leftAnchor.constraint(equalTo: lblTitle.leftAnchor).isActive = true
        imgVFeed.rightAnchor.constraint(equalTo: lblTitle.rightAnchor).isActive = true
        imgVFeed.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: (16.0).proportionalToWidth).isActive = true
        imgVFeed.heightAnchor.constraint(equalToConstant: (200.0).proportionalToWidth).isActive = true
    }
    
    private func setupDetail() {
        
        lblDetail.font = HelveticaNeue.regular.size((15.0).proportionalToWidth)
        lblDetail.translatesAutoresizingMaskIntoConstraints = false
        lblDetail.numberOfLines = 0
        contentView.addSubview(lblDetail)
        
        lblDetail.leftAnchor.constraint(equalTo: imgVFeed.leftAnchor).isActive = true
        lblDetail.rightAnchor.constraint(equalTo: imgVFeed.rightAnchor).isActive = true
        lblDetail.topAnchor.constraint(equalTo: imgVFeed.bottomAnchor, constant: (16.0).proportionalToWidth).isActive = true
        lblDetail.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: (-16.0).proportionalToWidth).isActive = true
    }
}

//
//  FeedViewModel.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import RxSwift

final class FeedViewModel {
    
    private let disposeBag: DisposeBag = DisposeBag()
    
    func getFeed() -> Observable<FeedModel> {
        return FeedInteractor.getAllFeed().asObservable()
    }
}

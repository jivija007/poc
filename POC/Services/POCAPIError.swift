//
//  POCAPIError.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation

enum POCAPIError: Error {
    
    case notEnabled
    case invalidJSONFormat
    case noInternet
    case customerError(code: Int, message: String)
}

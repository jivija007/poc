//
//  PrimtiviewSequence+Response.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Moya
import RxSwift

extension PrimitiveSequence where Trait == SingleTrait, Element == Response {
    
    func mapObject<T: Codable>(type: T.Type) -> Single<T> {
        
        return flatMap { response in
            let model = try self.handleData(response: response, type: type)
            return Single.just(model)
        }
    }
    
    func mapSuccessful() -> Single<Bool> {
        
        return flatMap { response in
            _ = try self.handleCode(response: response, type: Bool.self)
            return Single.just(true)
        }
    }
}

extension PrimitiveSequence {
    
    private func handleCode<T: Codable>(response: Response, type: T.Type, isMapSucess: Bool = false) throws -> Response {
        
        print("statusCode===\(response.statusCode)")
        guard let tempResponse = try? response.filterSuccessfulStatusCodes() else {
            
            switch response.statusCode {
            default:
                throw POCAPIError.notEnabled
            }
        }
        
        return tempResponse
    }
    
    private func handleData<T: Codable>(response: Response, type: T.Type) throws -> T {
        
        let tempResponse = try self.handleCode(response: response, type: type.self)
        let jsondata = try handleResponse(response: tempResponse, type: type)
        
        return jsondata
    }
    
    private func handleResponse<T: Codable>(response: Response, type: T.Type) throws -> T {

        guard let stringData = String(data: response.data, encoding: .isoLatin1),
            let data = stringData.data(using: .utf8),
            let result = try? JSONDecoder().decode(T.self, from: data) else {
            throw POCAPIError.invalidJSONFormat
        }
        return result
    }
}

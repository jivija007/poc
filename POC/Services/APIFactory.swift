//
//  APIFactory.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import Moya

enum APIFactory {
    case allFeed
}

extension APIFactory: TargetType {
    
    var headers: [String: String]? {
        return nil
    }
    
    var baseURL: URL {
        return URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")!
    }
    
    var path: String {
        
        switch self {
        case .allFeed:
            return ""
        }
    }
    
    var method: Moya.Method {
        
        switch self {
        case .allFeed:
            return .get
        }
    }
    
    var sampleData: Data { return Data(base64Encoded: "just for test")! }
    
    var task: Task {
        
        switch self {
        case .allFeed:
            return .requestPlain
        }
    }
    
    private func changeToMultipartFormDataType( _ string: String, name: String) -> MultipartFormData {
        return MultipartFormData(provider: .data(string.data(using: .utf8)!), name: name)
    }
}

extension APIFactory {
    
    static func httpHeaders() -> [String: String] {
        
        return [
            "Content-type": "application/json",
        ]
    }
}

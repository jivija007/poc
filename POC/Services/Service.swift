//
//  Service.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import RxSwift

class Service {
    
    static let shared = Service()
    static private func getNetworkManager() -> Alamofire.SessionManager? {
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10.0
        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }
    
    private let provider: MoyaProvider<APIFactory>
    
    let endpointClosure = { (target: APIFactory) -> Endpoint in
        
        let url = target.baseURL.appendingPathComponent(target.path).absoluteString
        let endpoint = Endpoint(
            url: url,
            sampleResponseClosure: { .networkResponse(200, target.sampleData) },
            method: target.method,
            task: target.task,
            httpHeaderFields: target.headers
        )
        
        return endpoint
    }
    
    let requestClosure = { (endpoint: Endpoint, done: @escaping MoyaProvider<APIFactory>.RequestResultClosure) in
        
        guard var request = try? endpoint.urlRequest() else {
            return
        }
        request.timeoutInterval = 20
        done(.success(request))
    }
    
    init() {
        
        let networkManager = Service.getNetworkManager()
        provider = MoyaProvider(endpointClosure: endpointClosure, requestClosure: requestClosure, manager: networkManager ?? MoyaProvider<APIFactory>.defaultAlamofireManager())
    }
}

extension Service {
    
    func connect(api: APIFactory) -> Single<Response> {
        return provider.rx.request(api)
    }
    
    func connectToProgress(api: APIFactory, progressBlock: ((Double) -> Void)? = nil) -> Single<Response> {
        
        return Single.create { [weak self] single in
            let cancellableToken = self?.provider.request(api, callbackQueue: nil, progress: { progressResponse in
                progressBlock?(progressResponse.progress)
            }, completion: { result in
                switch result {
                case let .success(response):
                    single(.success(response))
                case let .failure(error):
                    single(.error(error))
                }
            })
            
            return Disposables.create {
                cancellableToken?.cancel()
            }
        }
    }
    
    func cancelAllRequest() {
        
        provider.manager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
}

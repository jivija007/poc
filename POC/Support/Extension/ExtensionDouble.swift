//
//  ExtensionDouble.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    
    var proportionalToWidth: CGFloat {
        return (Device.screenWidth * CGFloat((self / 414.0)))
    }
    
    var proportionalToHeight: CGFloat {
        return (Device.screenHeight * CGFloat((self / 896.0)))
    }
}

//
//  Color.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static let white_ffffff = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let white_EBEBEB = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
    static let black_000000 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let primary = UIColor(red: 0.22, green: 0.58, blue: 0.29, alpha: 1.0)
    static let secondary = UIColor.lightGray
    static let theme = UIColor.orange
}

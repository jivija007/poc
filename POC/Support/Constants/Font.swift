//
//  Font.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import UIKit

enum HelveticaNeue {
    
    case regular
    case bold
    case boldItalic
    case condensedBlack
    case condensedBold
    case italic
    case light
    case lightItalic
    case medium
    case mediumItalic
    case thin
    case thinItalic
    case ultraLight
    case ultraLightItalic
    
    func size(_ size: CGFloat) -> UIFont {
        
        switch self {
        case .regular:
            return UIFont(name: "HelveticaNeue", size: size)!
        case .bold:
            return UIFont(name: "HelveticaNeue-Bold", size: size)!
        case .boldItalic:
            return UIFont(name: "HelveticaNeue-BoldItalic", size: size)!
        case .condensedBlack:
            return UIFont(name: "HelveticaNeue-CondensedBlack", size: size)!
        case .condensedBold:
            return UIFont(name: "HelveticaNeue-CondensedBold", size: size)!
        case .italic:
            return UIFont(name: "HelveticaNeue-Italic", size: size)!
        case .light:
            return UIFont(name: "HelveticaNeue-Light", size: size)!
        case .lightItalic:
            return UIFont(name: "HelveticaNeue-LightItalic", size: size)!
        case .medium:
            return UIFont(name: "HelveticaNeue-Medium", size: size)!
        case .mediumItalic:
            return UIFont(name: "HelveticaNeue-MediumItalic", size: size)!
        case .thin:
            return UIFont(name: "HelveticaNeue-Thin", size: size)!
        case .thinItalic:
            return UIFont(name: "HelveticaNeue-ThinItalic", size: size)!
        case .ultraLight:
            return UIFont(name: "HelveticaNeue-UltraLight", size: size)!
        case .ultraLightItalic:
            return UIFont(name: "HelveticaNeue-UltraLightItalic", size: size)!
        }
    }
}


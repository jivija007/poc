//
//  Device.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation
import UIKit

final class Device {
    
    class var screen: UIScreen {
        return UIScreen.main
    }
    
    class var screenBounds: CGRect {
        return screen.bounds
    }
    
    class var screenOrigin: CGPoint {
        return screenBounds.origin
    }
    
    class var screenSize: CGSize {
        return screen.bounds.size
    }
    
    class var screenX: CGFloat {
        return screenOrigin.x
    }
    
    class var screenY: CGFloat {
        return screenOrigin.y
    }

    class var screenWidth: CGFloat {
        return screenSize.width
    }

    class var screenHeight: CGFloat {
        return screenSize.height
    }
    
    class var screenCenter: CGPoint {
        return CGPoint(x: screenWidth / 2.0, y: screenHeight / 2.0)
    }
    
    class var screenCenterX: CGFloat {
        return screenCenter.x
    }
    
    class var screenCenterY: CGFloat {
        return screenCenter.y
    }
    
    class var navigationBarHeight: CGFloat {
        return UINavigationBar.appearance().frame.height
    }
    
    class var tabBarHeight: CGFloat {
        return UITabBar.appearance().frame.height
    }

    class var isIOS14: Bool {
        
        if #available(iOS 14, *) {
            return true
        }
        return false
    }
    
    class var isIOS13: Bool {
        
        if #available(iOS 13, *) {
            return true
        }
        return false
    }
    
    class var isIOS12: Bool {
        
        if #available(iOS 12, *) {
            return true
        }
        return false
    }
    
    class var isIOS11: Bool {
        
        if #available(iOS 11, *) {
            return true
        }
        return false
    }
    
    class var isIOS10: Bool {
        
        if #available(iOS 10, *) {
            return true
        }
        return false
    }
    
    class var isIOS9: Bool {
        
        if #available(iOS 9, *) {
            return true
        }
        return false
    }

    class var userInterfaceIdiom: UIUserInterfaceIdiom {
        return UIDevice.current.userInterfaceIdiom
    }
    
    class var isIPhone: Bool {
        return userInterfaceIdiom == .phone
    }
    
    class var isIPad: Bool {
        return userInterfaceIdiom == .pad
    }
    
    class var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    class var isIPhone5SE: Bool {
        return screenHeight == 568
    }
    
    class var isIPhone8: Bool {
        return screenHeight == 667
    }
    
    class var isIPhone8Plus: Bool {
        return screenHeight == 736
    }
    
    class var isIPhone11: Bool {
        return screenHeight == 896
    }
    
    class var isIPhone11Pro: Bool {
        return screenHeight == 812
    }
    
    class var isIPhone11ProMax: Bool {
        return screenHeight == 896
    }
    
    class var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }
    
    class var isPortrait: Bool {
        return orientation.isPortrait
    }
    
    class var isLandscape: Bool {
        return orientation.isLandscape
    }
    
    class var name: String {
        return UIDevice.current.name
    }
    
    class var model: String {
        return UIDevice.current.model
    }
    
    class var systemName: String {
        return UIDevice.current.systemName
    }
    
    class var systemVersion: String {
        return UIDevice.current.systemVersion
    }
    
    class var identifierForVendor: String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    class var batteryLevel: Float {
        return UIDevice.current.batteryLevel
    }
}

//
//  Log.swift
//  POC
//
//  Created by mac-0007 on 08/11/20.
//  Copyright © 2020 mac-0007. All rights reserved.
//

import Foundation

public func Log<T>(_ object: T?, filename: String = #file, line: Int = #line, funcname: String = #function) {
    
    #if DEBUG
    
        guard let object = object else { return }
        print("***** \(Date()) \(filename.components(separatedBy: "/").last ?? "") (line: \(line)) :: \(funcname) :: \(object)")
    #endif
}
